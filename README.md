# Welcome to fadeR

Please note that everything was created to evaluate measurements that were recorded using the *Analyst* software from *Sciex*.

## Requirements

You will need R for running fadeR. This script has been tested with:
- R v4.0.3,
- RStudio v1.3.959, and 
- RTools v4.0
	
The mzML files were converted from .wiff files using
	ProteoWizard v3.0.19276-7124c5404, which can be downloaded from  [here](https://proteowizard.sourceforge.io/download.html)

fadeR requires the following R libraries:
- stringr, 
- mzR, 
- MSnbase, 
- DescTools, 
- httr, 
- htmlTable, 
- emayili, 
- dplyr, 
- lubridate, 
- ggplot2, 
- ggpubr, 
- gridExtra, 
- XML, 
- MassSpecWavelet, 
- optparse

If you have problems installing *mzR*, please follow these [instructions](https://bioconductor.org/packages/release/bioc/html/mzR.html)

If you have problems installing *MSnbase*, please follow these [instructions](https://bioconductor.org/packages/release/bioc/html/MSnbase.html)



## Customization

fadeR script will read input data from the folders: *temp_data_wiff* and *temp_data* by default. Make sure these folders are created and place your .wiff data files from Analyst in "temp_data_wiff". If you wish to use a different folder, please change the folder location in the "auto_run_wiff" script, as follows: Open the "auto_run_wiff" and change the folder locations for the .wiff data. Please be aware to use / instead of \.

- .wiff file location in line 4.

fadeR cannot read .wiff data files, therefore, .wiff data files need to be changed into mzML files by using the software msconvert of ProteoWizard. The software can be used as follows: Open the "auto_run_wiff" and change the folder location for the software msconvert of ProteoWizard. Please be aware to use / instead of \.

- msconvert location in line 5.

Please include your target analytes in the "system_lcms_manual_is_target_input" script, according to the carbamazepine (CBZ) example

fadeR has two modes of operation. It can process all files found in a folder, or it can keep (indefinitively) looking for new files in a folder until *manually* terminated. If you want to to process a set of files only once, please set the "run_once" parameter in the "auto_run_wiff" script to TRUE. Setting it to FALSE will keep fadeR in an infinite loop looking and processing new files as they appear.

fadeR can be executed by running the "auto_run_wiff" script. 

Your results are shown in a table. You can dowload your file in a .txt file using the following code in  the console: 
`write.table(lcms_data, "test.txt", sep="\t")`, where *lcms_data* stores the returned value of the "auto_run_wiff" script.

## Alarm triggering (optional)

If you want to use the alarm trigger of fadeR, you have to include thresholds and substance information in the scripts "system_lcms_alarm_thresholds" and "system_lcms_manual_alarm_thresholds", according to the CBZ example.

When you want to use the alarm tool, please enter your email adress in line 195 in the "system_lcms_comm" script. 
Note: You need to enter an active server in line 54 in the same script.